import numpy as np
import matplotlib.pyplot as plt
from random import random
from scipy.signal import get_window, find_peaks
import soundfile as sf
from functions import shortTimeEnergy, zeroCrossingRate, ACF, AMDF, formants
import os
import csv

# CREATE THE CSV FILE
with open('DB/database.csv', mode='w') as employee_file:
	employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

	audio_name = ['01a_duda.wav', '01b_duda.wav', '01c_duda.wav', '02a_Rayssa.wav', '02b_Rayssa.wav', '02c_Rayssa.wav', '03a_Walisson.wav', '03b_Walisson.wav', '03c_Walisson.wav', '04a_Alysson.wav', '04b_Alysson.wav', '04c_Alysson.wav', '05a_Igor.wav', '05b_Igor.wav', '05c_Igor.wav', '06a_Nery.wav', '06b_Nery.wav', '06c_Nery.wav']
	energy_thres = 0.1 # without pre-enphasis
	n_peaks_rest = 0

	#for audio in range(0, 1):
	for audio in range(len(audio_name)):
		signal, samplerate = sf.read('audios/' + audio_name[audio])
		sampletime = 1 / samplerate
		time = np.arange(0, len(signal) * sampletime, sampletime)

		# Definindo variáveis
		overlap = 0.5
		frame_time = 0.03
		samples_frame = int(frame_time * samplerate)
		shift_frame = int(samples_frame * overlap)
		qtd_frames = int((len(signal) * sampletime) // (frame_time * overlap))

		print('Number of frames:', (len(signal) * sampletime) // (frame_time * overlap))

		# Criando arrays para armazenar Energia, Magnitude e TCZ
		frame_energy = []
		frame_tcz = []
		pitch_acf = []
		pitch_amdf = []
		n_form = 6
		frame_formants = np.zeros((qtd_frames, n_form))


		# Applying the pre-emphasis on the signal
		pre_emphasis = np.zeros(len(signal))
		pre_emphasis[0] = signal[0]
		for j in range(1, len(signal)):
			pre_emphasis[j] = signal[j] - (0.95 * signal[j-1])

		plt.style.use('bmh')
		'''plt.ion()
		plt.figure(figsize=(16,8))'''

		it = 0
		for i in range(0, len(signal), shift_frame):
			os.system('clear')
			print('Áudio:', audio_name[audio])
			print('%.2f' % ((it / qtd_frames) * 100))

			frame = signal[i:i + samples_frame]
			frame_pre = pre_emphasis[i:i + samples_frame]

			# Complete the last frame with zeros
			if (len(frame) != samples_frame):
				frame = np.concatenate([frame, np.zeros(samples_frame - len(frame))])

			# Hamming Window
			frame_original = frame # frame without application of the Hamming window
			frame = frame * get_window('hamming', len(frame))
			frame_pre = frame_pre * get_window('hamming', len(frame_pre))

			frame_energy.append(shortTimeEnergy(frame)) # Energy
			
			if (frame_energy[-1] > energy_thres):
				frame_tcz.append(zeroCrossingRate(frame)) # ZCR
				frame_amdf = AMDF(frame) # AMDF

				'''peaks, peak_heights = find_peaks(frame_acf, height=0)
				peaks_sort = np.argsort(peak_heights['peak_heights'])
				p1, p2 = peaks_sort[-1], peaks_sort[-2]'''
				p_amdf = np.argmin(frame_amdf[len(frame)+10:len(frame) + 200]) + 10

				#p_acf_current = (samplerate / abs(kappa[peaks[p2]] - kappa[peaks[p1]]))
				p_amdf_current = (samplerate / p_amdf)

				#if (p_acf_current < 500):
				#	pitch_acf.append(p_acf_current)
				if (p_amdf_current < 500):
					pitch_amdf.append(p_amdf_current)

					'''plt.clf()
					plt.cla()
					plt.subplots_adjust(hspace=0.6)
					plt.subplot(211)
					plt.title('ACF')
					plt.plot(peaks, frame_acf[peaks], 'rx')
					#plt.plot(peaks[p1], peak_heights['peak_heights'][p1], 'rx')
					#plt.plot(peaks[p2], peak_heights['peak_heights'][p2], 'rx')
					plt.plot(np.arange(len(frame_acf)), frame_acf, 'b')        
					plt.subplot(212)
					plt.title('AMDF')
					plt.plot(np.arange(len(frame_amdf)), frame_amdf, 'g')
					p_amdf += len(frame)
					plt.plot(len(frame), frame_amdf[len(frame)], 'rx')
					plt.plot(p_amdf, frame_amdf[p_amdf], 'rx')
					if (i == 20 * shift_frame):
						print('Opa!')
						plt.savefig('Resultados/acf_amdf_' + str(audio) + '.png')
					plt.pause(0.1)'''

					#print('Pitch through AMDF Method:', pitch_amdf[-1])

				if (len(pitch_amdf) > 0):
					n_peaks = (frame_time + n_peaks_rest) // (1/pitch_amdf[-1])
					n_peaks_rest = (frame_time + n_peaks_rest) % (1 / pitch_amdf[-1])
					
					formants(frame_pre, samplerate, n_form)
					ffreq = formants(frame_pre, samplerate, n_form)
					frame_formants[it] = ffreq
					
					employee_writer.writerow([str(pitch_amdf[-1]), str(ffreq[0]), str(ffreq[1]), str(ffreq[2]), str(ffreq[3]), str(ffreq[4]), str(ffreq[5]), str(int(audio_name[audio][:2]) - 1)])

			it += 1

		'''print('- Pitch Mean (ACF):', np.mean(pitch_acf))
		#print('- Standard Deviation (ACF):', np.std(pitch_acf))
		print('- Pitch Mean (AMDF):', np.mean(pitch_amdf))
		#print('- Standard Deviation (AMDF):', np.std(pitch_amdf))
		#print('First formant:', np.mean(frame_formants[:it,0]))
		#print('Second formant:', np.mean(frame_formants[:it,1]))
		#print('Third formant:', np.mean(frame_formants[:it,2]))
		#print('Quarter formant:', np.mean(frame_formants[:it,3]))
		#print('Fifth formant:', np.mean(frame_formants[:it,4]))
		print('Formants: %.6f, %.6f, %.6f, %.6f' % (np.mean(frame_formants[:it,0]), np.mean(frame_formants[:it,1]), np.mean(frame_formants[:it,2]), np.mean(frame_formants[:it,3])))'''

		#plt.ioff()