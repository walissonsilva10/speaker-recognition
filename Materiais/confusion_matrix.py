# Tutorial: https://towardsdatascience.com/deep-learning-with-python-703e26853820
# Diabets Dataset: https://www.kaggle.com/kumargh/pimaindiansdiabetescsv

# Importing Keras Sequential Model
from keras.models import Sequential, model_from_json
from keras.layers import Dense
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical
from sklearn.utils import shuffle
import numpy as np 
from sklearn.metrics import confusion_matrix

# Loading the data set (PIMA Diabetes Dataset)
dataset = np.loadtxt('DB/database.csv', delimiter=",")

# Loading the input values to X and Label values Y using slicing.
X = dataset[:546, 0:7]
Y = dataset[:546, 7]

# Enconder the output
# Look the topic five in this link for more details: https://machinelearningmastery.com/multi-class-classification-tutorial-keras-deep-learning-library/
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
Y = np_utils.to_categorical(encoded_Y)

####  Load Model of DNN from JSON file ####
json_file = open('model_new.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model_new.h5")
print("Loaded model from disk\n")

# Compiling the model
loaded_model.compile(loss='categorical_crossentropy',
              optimizer='adamax', metrics=['accuracy'])
scores = loaded_model.evaluate(X, Y)

print("%s: %.2f%%" % (loaded_model.metrics_names[1], scores[1] * 100))

predict = loaded_model.predict(X)

print(predict.shape, Y.shape)

# from categorial to lable indexing
y_pred = predict.argmax(1)
Y = Y.argmax(1)

print(y_pred.shape, Y.shape)

print(confusion_matrix(y_pred, Y))