# Tutorial: https://towardsdatascience.com/deep-learning-with-python-703e26853820
# Diabets Dataset: https://www.kaggle.com/kumargh/pimaindiansdiabetescsv

# Importing Keras Sequential Model
from keras.models import Sequential, model_from_json
from keras.layers import Dense
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical
from sklearn.utils import shuffle
import numpy as np 

# Define input and output numbers
n_out = 4 # Defined by number of people
n_in = 7 # Defined by number of features

# Initializing the seed value to a integer.
seed = 7

np.random.seed(seed)

# Loading the data set (PIMA Diabetes Dataset)
dataset = np.loadtxt('DB/database.csv', delimiter=",")

# Loading the input values to X and Label values Y using slicing.
X = dataset[:367, 0:n_in]
Y = dataset[:367, n_in]

# Enconder the output
# Look the topic five in this link for more details: https://machinelearningmastery.com/multi-class-classification-tutorial-keras-deep-learning-library/
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
Y = np_utils.to_categorical(encoded_Y)

# Initializing the Sequential model from KERAS.
model = Sequential()

# Creating a 16 neuron hidden layer with Linear Rectified activation function.
model.add(Dense(14, input_dim=n_in, init='uniform', activation='relu'))

# Creating a 8 neuron hidden layer.
model.add(Dense(7, init='uniform', activation='relu'))

# Adding a output layer.
model.add(Dense(n_out, init='uniform', activation='softmax'))

# Compiling the model
model.compile(loss='categorical_crossentropy',
              optimizer='adamax', metrics=['accuracy'])
# Fitting the model
model.fit(X, Y, nb_epoch=400, batch_size=5)#, validation_split=0.1)

scores = model.evaluate(X, Y)

print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))


# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

# serialize weights to HDF5
model.save_weights("model.h5")

print("Saved model to disk")

'''predictions = np.array(
	[[80.40201005025126,538.1702970279027,704.5031043349951,1385.6109593058327,2624.097452535812],
	[266.6666666666667,392.69497589866387,902.7241186499493,1615.9306043293363,3427.7732849452286]]).reshape(-1,5)

#predictions = np.array([266.6666666666667,392.69497589866387,902.7241186499493,1615.9306043293363,3427.7732849452286]).reshape(-1, 5)

proba = model.predict(predictions)

print(proba)

print('\n\n--- PROBABILITY FOR EACH PERSON ---\n')
for j in range(proba.shape[0]):
	for i in range(proba.shape[1]):
		print(' - Person %d: %.10f' % (i + 1, proba[j,i]))
	print()'''