# http://bastian.rieck.ru/blog/posts/2014/simple_experiments_speech_detection/

import numpy as np
import matplotlib.pyplot as plt
from random import random
from scipy.spatial.distance import euclidean
import soundfile as sf
from scipy.signal import get_window, find_peaks

# Energia do Sinal
def shortTimeEnergy(frame):
	return sum([ abs(x)**2 for x in frame ])# / len(frame)

# Magnitude do sinal
def shortTimeMagnitude(frame):
	return sum([ abs(x) for x in frame ])# / len(frame)

# Taxa de Cruzamentos por Zero
def zeroCrossingRate(frame):
    signs = np.sign(frame)
    signs[signs == 0] = -1

    return len(np.where(np.diff(signs))[0])# / len(frame)

# Carregando o sinal de voz
signal, samplerate = sf.read('../../audios/05a_Igor.wav')
sampletime = 1 / samplerate
#signal = signal[2*samplerate:]#3*samplerate]
time = np.arange(0, len(signal) * sampletime, sampletime)
time = time[:len(signal)]

# Definindo variáveis
overlap = 0.5
frame_time = 0.03
samples_frame = int(frame_time * samplerate)
shift_frame = int(samples_frame * overlap)
qtd_frames = len(signal) * sampletime * frame_time * overlap
k_means = True

# Criando arrays para armazenar Energia, Magnitude e TCZ
frame_energy = []
frame_tcz = []
frame_magnitude = []
voiced = []
voz_silencio = []

limiar_energia = 3
limiar_tcz = 100
limiar_energia2 = 1
limiar_tcz2 = 40

for i in range(0, len(signal), shift_frame):
    frame = signal[i:i + samples_frame]
    frame = frame * get_window('hamming', len(frame))
    frame_energy.append(shortTimeEnergy(frame))
    frame_magnitude.append(shortTimeMagnitude(frame))
    frame_tcz.append(zeroCrossingRate(frame))

    if (frame_energy[-1] > limiar_energia):
        if (frame_tcz[-1] > limiar_tcz):
            voiced.append(0)
        else:
            voiced.append(np.max(signal))
    else:
    	voiced.append(0)

    if (frame_energy[-1] < limiar_energia2 and frame_energy[-1] < limiar_tcz2):
        voz_silencio.append(0)
    else:
        voz_silencio.append(np.max(signal))

frame_energy = np.array(frame_energy)
frame_magnitude = np.array(frame_magnitude)
frame_tcz = np.array(frame_tcz)
frames = np.arange(0, len(signal) * sampletime, shift_frame * sampletime)

plt.figure(0)
plt.subplots_adjust(hspace=0.4)
plt.subplot(211)
plt.title('Sinal Original')
plt.plot(time, signal)
plt.subplot(212)
plt.title('Energia Segmental')
plt.ylabel('Amplitude')
plt.xlabel('Tempo (s)')
plt.plot(frames, frame_energy)

plt.figure(1)
plt.subplots_adjust(hspace=0.4)
plt.subplot(211)
plt.title('Sinal Original')
plt.plot(time, signal)
plt.subplot(212)
plt.title('Magnitude Segmental')
plt.ylabel('Amplitude')
plt.xlabel('Tempo (s)')
plt.plot(frames, frame_magnitude)

plt.figure(2)
plt.subplots_adjust(hspace=0.4)
plt.subplot(211)
plt.title('Sinal Original')
plt.plot(time, signal)
plt.subplot(212)
plt.title('Taxa de Cruzamentos por Zero (TCZ)')
plt.ylabel('Amplitude')
plt.xlabel('Tempo (s)')
plt.plot(frames, frame_tcz)
plt.show()