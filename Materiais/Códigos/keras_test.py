# Tutorial: https://towardsdatascience.com/deep-learning-with-python-703e26853820
# Diabets Dataset: https://www.kaggle.com/kumargh/pimaindiansdiabetescsv

# Importing Keras Sequential Model
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical
from sklearn.utils import shuffle
import numpy as np 

# Initializing the seed value to a integer.
seed = 7

np.random.seed(seed)

# Loading the data set (PIMA Diabetes Dataset)
dataset = np.loadtxt('DB/pima-indians-diabetes.csv', delimiter=",")

# Loading the input values to X and Label values Y using slicing.
X = dataset[:, 0:8]
Y = dataset[:, 8]

# Initializing the Sequential model from KERAS.
model = Sequential()

# Creating a 16 neuron hidden layer with Linear Rectified activation function.
model.add(Dense(16, input_dim=8, init='uniform', activation='relu'))

# Creating a 8 neuron hidden layer.
model.add(Dense(8, init='uniform', activation='relu'))

# Adding a output layer.
model.add(Dense(1, init='uniform', activation='sigmoid'))

# Compiling the model
model.compile(loss='binary_crossentropy',
              optimizer='adam', metrics=['accuracy'])
# Fitting the model
model.fit(X, Y, nb_epoch=50, batch_size=10)

scores = model.evaluate(X, Y)

print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

predictions = np.array([6,148,72,35,0,33.6,0.627,50]).reshape(-1,8)

proba = model.predict_classes(predictions)[0]

print(proba)

#for i, j in zip(prediction_ , predict_labels):
#    print( " the nn predict {}, and the species to find is {}".format(i,j))

#print('%s' % model.predict_classes(test))
