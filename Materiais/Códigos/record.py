import sounddevice as sd
import matplotlib.pyplot as plt 
import numpy as np 

duration = 2.5  # seconds
fs = 22050

myrecording = sd.rec(int(duration * fs), samplerate=fs, channels=1)
sd.wait()

time = np.linspace(0, duration, duration * fs, endpoint=False)

plt.style.use('bmh')
plt.plot(time, myrecording)
plt.show()