import numpy as np
import matplotlib.pyplot as plt
from random import random
from scipy.spatial.distance import euclidean
from scipy.signal import get_window, find_peaks
import soundfile as sf
from functions import shortTimeEnergy, zeroCrossingRate, ACF, AMDF
from audiolazy import lpc


x = [1, -2, 3, -4, -3, 2, -3, 2, 1]
a = lpc(x, 3)
samplerate = 2
print('LPC Filter:', a)

poly = []

for i in range(len(list(a)[0])):
	poly.append(list(a)[0][i])

r = np.roots(poly)
r = r[r.imag > 0.01]

ffreq = np.sort(np.arctan2(r.imag, r.real) * samplerate / (2 * np.pi)) 

print('\n### FORMANTS ###')
for i in range(len(ffreq)):
	print('Formant = %d | Frequency = %.2f' % (i + 1, ffreq[i]))