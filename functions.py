import numpy as np
import matplotlib.pyplot as plt
from random import random
from scipy.spatial.distance import euclidean
from scipy.signal import get_window, find_peaks
import soundfile as sf
from audiolazy import lpc

# Energia do Sinal
def shortTimeEnergy(frame):
	return sum([ abs(x)**2 for x in frame ])# / len(frame)

# Taxa de Cruzamentos por Zero
def zeroCrossingRate(frame):
    signs = np.sign(frame)
    signs[signs == 0] = -1

    return len(np.where(np.diff(signs))[0])

# Autocorrelação do Sinal
def ACF(signal):
    K = len(signal)

    # compute and truncate ACF
    acf = 1/len(signal) * np.correlate(signal, signal, mode='full')
    acf = acf[(len(signal)-1)-(K-1):(len(signal)-1)+K]
    kappa = np.arange(-(K-1), K)

    return kappa, acf

def AMDF(signal):
    N = len(signal)
    amdf = np.zeros(2*N - 1)

    for m in range(0, 2*N - 1):
        if (m < N):
            for n in range(0, m + 1):
                amdf[m] += abs(signal[n] - signal[-m - 1 + n])
        else:
            for n in range(m - N, N):
                amdf[m] += abs(signal[n] - signal[n - m + N])

    return amdf / N

def formants(frame, fs, n_form):
    ncoeff = int(2 + (fs / 1000))

    frame = frame - np.mean(frame)
    frame /= np.max(frame)

    a = lpc(frame, ncoeff)
    #print('LPC Filter:', a)
    poly = []

    for i in range(len(list(a)[0])):
        poly.append(list(a)[0][i])

    r = np.roots(poly)
    r = r[r.imag > 0.01]

    ffreq = np.sort(np.arctan2(r.imag, r.real) * fs / (2 * np.pi)) 

    return ffreq[:n_form]

def jitter(frame, n_peaks):
    n_peaks = int(n_peaks)
    peaks, peak_heights = find_peaks(frame, height=0)
    peaks_sort = np.argsort(peak_heights['peak_heights'])
    peaks_sort = peaks_sort[-1:-n_peaks-1:-1]
    peaks_sort = np.sort(peaks_sort)

    plt.ion()
    plt.clf()
    plt.cla()
    plt.plot(np.arange(len(frame)), frame)
    soma = 0
    for peak in range(n_peaks):
        soma += peaks[peaks_sort[peak]] * sampletime
        plt.plot(peaks[peaks_sort[peak]], peak_heights['peak_heights'][peaks_sort[peak]], 'rx')
    plt.pause(0.5)

    jitter = 0
    for i in range(1, n_peaks):
        jitter += ((peaks[peaks_sort[i]] * sampletime) - (peaks[peaks_sort[i-1]] * sampletime))

    jitter /= (n_peaks - 1)

    return jitter / (soma / n_peaks)