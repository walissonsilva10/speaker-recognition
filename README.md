# Speech Recognition through Pitch and Formants based on DNN

- Frases foneticamente balanceadas.

## Requiriments

You must install the `audiolazy` library. For this,

> `pip3 install audiolazy`

And more:

> `pip3 install PySoundFile sklearn tensorflow keras pyttsx3 sounddevice h5py`

### References

- https://shrikar.com/deep-learning-with-keras-and-python-for-multiclass-classification/
- https://machinelearningmastery.com/multi-class-classification-tutorial-keras-deep-learning-library/