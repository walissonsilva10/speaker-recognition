# Importing Keras Sequential Model
from keras.models import Sequential, model_from_json
from keras.layers import Dense
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical
from sklearn.utils import shuffle
import numpy as np
from scipy.signal import get_window, find_peaks
import soundfile as sf
from functions import shortTimeEnergy, zeroCrossingRate, ACF, AMDF, formants
import sounddevice as sd
import matplotlib.pyplot as plt 
from gtts import gTTS
import subprocess
import time

n_people = 4
people_name = ['Duda', 'Rayssa', 'Walisson', 'Alysson']
people_phonetics = ['Duda', 'Rayssa', 'Ualisson', 'Alisson']
recognition = np.zeros(n_people)

####  Load Model of DNN from JSON file ####
json_file = open('model_new.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model_new.h5")
print("Loaded model from disk\n")

audio_name = '04e_Alysson.wav'
energy_thres = 0.05
n_peaks_rest = 0

'''signal, samplerate = sf.read('audios/' + audio_name)
sampletime = 1 / samplerate
time = np.arange(0, len(signal) * sampletime, sampletime)'''

duration = 1.5  # seconds
fs = samplerate = 22050
sampletime = 1 / samplerate

print('Pode falar... ;)\n')
time.sleep(0.3)
signal = sd.rec(int(duration * fs), samplerate=fs, channels=1)
sd.wait()

time = np.linspace(0, duration, duration * fs, endpoint=False)
signal = signal[:,0]


plt.style.use('bmh')
plt.plot(time, signal)
plt.show()

# Definindo variáveis
overlap = 0.5
frame_time = 0.03
samples_frame = int(frame_time * samplerate)
shift_frame = int(samples_frame * overlap)
qtd_frames = int((len(signal) * sampletime) // (frame_time * overlap))

#print('Number of frames:', (len(signal) * sampletime) // (frame_time * overlap))

frame_energy = []
frame_tcz = []
pitch_amdf = []
n_form = 6
#frame_formants = np.zeros((qtd_frames, n_form))

# Applying the pre-emphasis on the signal
pre_emphasis = np.zeros(len(signal))
pre_emphasis[0] = signal[0]
for j in range(1, len(signal)):
	pre_emphasis[j] = signal[j] - (0.95 * signal[j-1])

it = 0
for i in range(0, len(signal), shift_frame):
	#os.system('clear')
	#print('%.2f' % ((it / qtd_frames) * 100))

	frame = signal[i:i + samples_frame]
	frame_pre = pre_emphasis[i:i + samples_frame]

	# Complete the last frame with zeros
	if (len(frame) != samples_frame):
		frame = np.concatenate([frame, np.zeros(samples_frame - len(frame))])

	# Hamming Window
	frame_original = frame # frame without application of the Hamming window
	frame = frame * get_window('hamming', len(frame))
	frame_pre = frame_pre * get_window('hamming', len(frame_pre))

	frame_energy.append(shortTimeEnergy(frame)) # Energy
	if (frame_energy[-1] > energy_thres):
		frame_tcz.append(zeroCrossingRate(frame)) # ZCR
		frame_amdf = AMDF(frame) # AMDF
		p_amdf = np.argmin(frame_amdf[len(frame)+10:len(frame) + 200]) + 10

		p_amdf_current = (samplerate / p_amdf)

		if (p_amdf_current < 500):
			pitch_amdf.append(p_amdf_current)

			n_peaks = (frame_time + n_peaks_rest) // (1/pitch_amdf[-1])
			n_peaks_rest = (frame_time + n_peaks_rest) % (1 / pitch_amdf[-1])
			
			ffreq = formants(frame_pre, samplerate, n_form)
			#frame_formants[it] = ffreq

			predictions = np.array([pitch_amdf[-1], ffreq[0], ffreq[1], ffreq[2], ffreq[3], ffreq[4], ffreq[5]]).reshape(-1, 7)

			proba = loaded_model.predict(predictions)

			print('\n\n--- PROBABILITY FOR EACH PERSON ---\n')
			for j in range(proba.shape[0]):
				for i in range(proba.shape[1]):
					print(' - Person %d: %.10f' % (i + 1, proba[j,i]))
				print()

			person_recognized = np.argmax(proba[0])
			if (proba[0][person_recognized] >= 0.99):
				recognition[person_recognized] += 1

	it += 1
 
print(recognition / np.sum(recognition))

recognition = recognition / np.sum(recognition)
recognition_sort = np.sort(recognition)

if (recognition_sort[-1] - recognition_sort[-2] > 0.5 and recognition_sort[-1] > 0.7):
	print('Olá, Pessoa ' + people_name[np.argmax(recognition)])
	voz = gTTS("Olá, " + people_phonetics[np.argmax(recognition)] + "! Pode entrar!", lang='pt')
	voz.save('audios/voice.mp3')
	subprocess.call(['mplayer', 'audios/voice.mp3'])
else:
	voz = gTTS("Quem é você?", lang='pt')
	voz.save('audios/voice.mp3')
	subprocess.call(['mplayer', 'audios/voice.mp3'])